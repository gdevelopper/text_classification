# Extraction et programmation statistique de l'information 
  **Classification de texte** 

## Contexte : 
Le projet suivant s'inscrit dans de module “d’extraction et programmation statistique de l’information” 
et qui vise à réaliser un profilage de la langue maternelle d'individus 
sur onze longues :
- Français 
- Chinois
- Italien
- Arabe
- Telugu
- Japonais
- Espagnole
- Hindi
- Coréen
- Allemand  
- Turque 
en se basant sur les textes écrits en anglais .
Nous avons à notre disposition un corpus de textes annoté ( confidencial ). 

### Notre approches de manière globale fut la suivante :

**Étape 1 (Transformation)** : 
Transformation des données en data frame pandas,avec chaque textes et son étiquette.

**Étape 2 (Exploration)** :
Nous avons chacun de son côté fait un inspection sur le jeux de données :
Nombre de textes au totale en chaque langues .
Nombre de phrases en totale et en moyennes en minimum sur chaque langues.
Nombre de mots en totale et en moyennes en minimum sur chaque langues.
Nombre de caractères spéciaux ```(! , ; ? + - )``` en chaque langues.
Répartition des nombres de mots/phrases sur toute les langue (pour voir quel langues avaient le plus de mots utilisés,
 en ayant constaté des similarités sur certaines langues )
Parcours des données à l’oeil nue pour observer des utilisation de certain mot spécifique au langage 
(ie : l'allemand prenant en compte une concaténation de mots et le considérant comme un seul mot 
ex : ```IThe``` , ```vogtlandexpress``` ...etc, nous avons aussi d’autre paternes de langues 
comme l’utilisation du ```cannot```, ```can not``` qui peuvent faire partir des critères de différenciation entre le 
chinois et le telugu qui sont très similaire, nous avons regardé la répartition de leurs utilisation sur les langues.
 
**Étape 3 (construction de jeux de données matriciels)**:
Nous avons construits des matrices binaires ```TF-IDF...``` 


**Etapes 4 (Analyse de corrélations)** 
Nous avons élaboré un jeux de données incluants les statistiques cités plus hauts en Étape 2 ,
et les données des matrices en Étape 3, puis on a analysé le à l’aide d’une analyse factoriel multiple (AFM) .
 
**Etapes 5 (Construction de modèles de classification)** 
 
**Etapes 6 (Evaluation des modéles)**
  
 
**Etapes 7(Conclusion)**

## Notation 

<img src="/images/Notation.JPG"></img>

## Autheurs

**DENANE Ghiles** 
## binary Matrix Only : 
ici j'utilise que l'ocurrance d'un mots dans le text:
<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.702</td><td>0.712</td><td>Binary Matrix Only</td><td>Logistic Regression</td><td>TRUE</td></tr>    
    <tr><td>0.675</td><td>0.689</td><td>Binary Matrix Only</td><td>Passivd Agressive Classifier</td><td>TRUE</td></tr>
    <tr><td>0.671</td><td>0.683</td><td>Binary Matrix Only</td><td>SVM LinearSVC</td><td>TRUE</td></tr>
    <tr><td>0.665</td><td>0.668</td><td>Binary Matrix Only</td><td>SGDClassifier</td><td>TRUE</td></tr>
    <tr><td>0.665</td><td>0.667</td><td>Binary Matrix Only</td><td>SVM NuSVC</td><td>TRUE</td></tr>
    <tr><td>0.217</td><td>0.665</td><td>Binary Matrix Only</td><td>Multinomial Bayes</td><td>TRUE</td></tr>
    <tr><td>0.648</td><td>0.635</td><td>Binary Matrix Only</td><td>Passivd Agressive Classifier</td><td>FALSE</td></tr>
    <tr><td>0.636</td><td>0.624</td><td>Binary Matrix Only</td><td>SVM LinearSVC</td><td>FALSE</td></tr>
    <tr><td>0.632</td><td>0.618</td><td>Binary Matrix Only</td><td>SGDClassifier</td><td>FALSE</td></tr>
    <tr><td>0.554</td><td>0.546</td><td>Binary Matrix Only</td><td>Logistic Regression</td><td>FALSE</td></tr>
    <tr><td>0.328</td><td>0.315</td><td>Binary Matrix Only</td><td>Bagging Classifier</td><td>TRUE</td></tr>
    <tr><td>0.197</td><td>0.25</td><td>Binary Matrix Only</td><td>Random Forest</td><td>TRUE</td></tr>
    <tr><td>0.184</td><td>0.247</td><td>Binary Matrix Only</td><td>Decision Tree</td><td>TRUE</td></tr>
    <tr><td>0.095</td><td>0.0790</td><td>Binary Matrix Only</td><td>SVM SVC</td><td>TRUE</td></tr>
</table>

## Binary Matrix Bigram Only : 
ici j'utilise l'occurrence des bigrams 
<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.724</td><td>0.717</td><td>Binary Matrix Bigram Only </td><td>Logistic Regression</td><td>TRUE</td></tr>
    <tr><td>0.715</td><td>0.712</td><td>Binary Matrix Bigram Only</td><td>SVM LinearSVC</td><td>TRUE</td></tr>
    <tr><td>0.709</td><td>0.706</td><td>Binary Matrix Bigram Only</td><td>Passivd Agressive Classifier</td><td>TRUE</td></tr>
    <tr><td>0.677</td><td>0.691</td><td>Binary Matrix Bigram Only</td><td>SVM NuSVC</td><td>TRUE</td></tr>
    <tr><td>0.686</td><td>0.674</td><td>Binary Matrix Bigram Only</td><td>SGDClassifier</td><td>TRUE</td></tr>
    <tr><td>0.281</td><td>0.647</td><td>Binary Matrix Bigram Only </td><td> Multinomial Bayes</td><td> TRUE</td></tr>
    <tr><td>0.648</td><td>0.638</td><td>Binary Matrix Bigram Only</td><td>Passivd Agressive Classifier</td><td>FALSE</td></tr>
    <tr><td>0.636 </td><td>0.624 </td><td>Binary Matrix Bigram Only </td><td>SVM LinearSVC </td><td>FALSE </td></tr>
    <tr><td>0.633 </td><td>0.62 </td><td>Binary Matrix Bigram Only </td><td>SGDClassifier </td><td>FALSE </td></tr>
    <tr><td>0.554 </td><td>0.546 </td><td>Binary Matrix Bigram Only </td><td>Logistic Regression </td><td>FALSE </td></tr>
    <tr><td>0.329 </td><td>0.316 </td><td>Binary Matrix Bigram Only </td><td>Bagging Classifier </td><td>TRUE </td></tr>
    <tr><td>0.204 </td><td>0.243 </td><td>Binary Matrix Bigram Only </td><td>Decision Tree </td><td>TRUE </td></tr>
    <tr><td>0.167 </td><td>0.230 </td><td>Binary Matrix Bigram Only </td><td>Random Forest </td><td>TRUE </td></tr>
</table>

## Binary Matrix Trigram Only
Occurance des trigrames

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.299</td><td>0.632</td><td>Binary Matrix trigram Only</td><td>Multinomial Bayes</td><td>VRAI</td></tr>
<tr><td>0.606</td><td>0.600</td><td>Binary Matrix trigram Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
    <tr><td>0.606</td><td>0.592</td><td>Binary Matrix trigram Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
    <tr><td>0.594</td><td>0.586</td><td>Binary Matrix trigram Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.583</td><td>0.575</td><td>Binary Matrix trigram Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
    <tr><td>0.389</td><td>0.41</td><td>Binary Matrix trigram Only</td><td>SVM NuSVC</td><td>VRAI</td></tr>
    <tr><td>0.149</td><td>0.274</td><td>Binary Matrix trigram Only</td><td>Random Forest</td><td>VRAI</td></tr>
    <tr><td>0.288</td><td>0.272</td><td>Binary Matrix trigram Only</td><td>Bagging Classifier</td><td>VRAI</td></tr>
    <tr><td>0.169</td><td>0.229</td><td>Binary Matrix trigram Only</td><td>Decision Tree</td><td>VRAI</td></tr>
</table>

## Count Matrix Only
Nombre d'occurance d'un mot.

<table>
   <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
   <tr><td> 0.692</td><td>0.691</td><td>Binary Matrix Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
     <tr><td>0.664</td><td>0.671</td><td>Binary Matrix Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.662</td><td>0.669</td><td>Binary Matrix Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
    <tr><td>0.242</td><td>0.643</td><td>Binary Matrix Only</td><td>Multinomial Bayes</td><td>VRAI</td></tr>
    <tr><td>0.652</td><td>0.631</td><td>Binary Matrix Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
    <tr><td>0.593</td><td>0.610</td><td>Binary Matrix Only</td><td>SVM NuSVC</td><td>VRAI</td></tr>
    <tr><td>0.319</td><td>0.309</td><td>Binary Matrix Only</td><td>Bagging Classifier</td><td>VRAI</td></tr>
    <tr><td>0.178</td><td>0.238</td><td>Binary Matrix Only</td><td>Random Forest</td><td>VRAI</td></tr>
    <tr><td>0.185</td><td>0.226</td><td>Binary Matrix Only</td><td>Decision Tree</td><td>VRAI</td></tr>
</table>

## Count Matrix Bigram Only 
Nombre d'ocurance des bigrammes.

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.704</td><td>0.704</td><td>Count Matrix Bigram Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
   <tr><td> 0.690</td><td>0.687</td><td>Count Matrix Bigram Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
    <tr><td>0.685</td><td>0.678</td><td>Count Matrix Bigram Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.657</td><td>0.667</td><td>Count Matrix Bigram Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
    <tr><td>0.300</td><td>0.661</td><td>Count Matrix Bigram Only</td><td>Multinomial Bayes</td><td>VRAI</td></tr>
    <tr><td>0.640</td><td>0.641</td><td>Count Matrix Bigram Only</td><td>SVM NuSVC</td><td>VRAI</td></tr>
    <tr><td>0.334</td><td>0.320</td><td>Count Matrix Bigram Only</td><td>Bagging Classifier</td><td>VRAI</td></tr>
    <tr><td>0.169</td><td>0.258</td><td>Count Matrix Bigram Only</td><td>Random Forest</td><td>VRAI</td></tr>
    <tr><td>0.207</td><td>0.254</td><td>Count Matrix Bigram Only</td><td>Decision Tree</td><td>VRAI</td></tr>
</table>


## Count Matrix Trigram Only  
Nombre d'ocurance des trigrammes

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.592</td><td>0.589</td><td>Count Matrix trigram Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
    <tr><td>0.599</td><td>0.58</td><td>Count Matrix trigram Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
    <tr><td>0.581</td><td>0.558</td><td>Count Matrix trigram Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.567</td><td>0.555</td><td>Count Matrix trigram Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
</table>

## TF-IDF Matrix Only  
TF-IDF des mots .

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.742</td><td>0.741</td><td>Binary Matrix Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
    <tr><td>0.733</td><td>0.73</td><td>Binary Matrix Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
    <tr><td>0.716</td><td>0.722</td><td>Binary Matrix Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.678</td><td>0.680</td><td>Binary Matrix Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
    <tr><td>0.176</td><td>0.491</td><td>Binary Matrix Only</td><td>Multinomial Bayes</td><td>VRAI</td></tr>
    <tr><td>0.333</td><td>0.365</td><td>Binary Matrix Only</td><td>SVM NuSVC</td><td>VRAI</td></tr>
    <tr><td>0.309</td><td>0.297</td><td>Binary Matrix Only</td><td>Bagging Classifier</td><td>VRAI</td></tr>
    <tr><td>0.184</td><td>0.221</td><td>Binary Matrix Only</td><td>Random Forest</td><td>VRAI</td></tr>
    <tr><td>0.179</td><td>0.208</td><td>Binary Matrix Only</td><td>Decision Tree</td><td>VRAI</td></tr>
</table>

## TF-IDF Matrix Bigram Only
TF-IDF des Bigrammes.

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
   <tr><td> 0.756</td><td>0.755</td><td>TF-IDF Matrix Bigram Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.749</td><td>0.747</td><td>TF-IDF Matrix Bigram Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
    <tr><td>0.748</td><td>0.737</td><td>TF-IDF Matrix Bigram Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
   <tr><td> 0.671</td><td>0.666</td><td>TF-IDF Matrix Bigram Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
    <tr><td>0.212</td><td>0.610</td><td>TF-IDF Matrix Bigram Only</td><td>Multinomial Bayes</td><td>VRAI</td></tr>
   <tr><td>0.408</td><td>0.414</td><td>TF-IDF Matrix Bigram Only</td><td>SVM NuSVC</td><td>VRAI</td></tr>
   <tr><td> 0.326</td><td>0.300</td><td>TF-IDF Matrix Bigram Only</td><td>Bagging Classifier</td><td>VRAI</td></tr>
    <tr><td>0.170</td><td>0.249</td><td>TF-IDF Matrix Bigram Only</td><td>Random Forest</td><td>VRAI</td></tr>
    <tr><td>0.183</td><td>0.233</td><td>TF-IDF Matrix Bigram Only</td><td>Decision Tree</td><td>VRAI</td></tr>
</table>

## TF-IDF Matrix Trigram Only
TF-IDF des Trigrammes

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td><td>Stopwords</td></tr>
    <tr><td>0.6478</td><td>0.636</td><td>TF-IDF Matrix trigram Only</td><td>Passivd Agressive Classifier</td><td>VRAI</td></tr>
    <tr><td>0.636</td><td>0.624</td><td>TF-IDF Matrix trigram Only</td><td>SVM LinearSVC</td><td>VRAI</td></tr>
   <tr><td> 0.632</td><td>0.622</td><td>TF-IDF Matrix trigram Only</td><td>SGDClassifier</td><td>VRAI</td></tr>
    <tr><td>0.554</td><td>0.546</td><td>TF-IDF Matrix trigram Only</td><td>Logistic Regression</td><td>VRAI</td></tr>
</table>

# Combinaison des Matrices précédentes 
## Binary Matrix words & bigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.761</td><td>0.766</td><td>Binary Matrix words & bigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.737</td><td>0.719</td><td>Binary Matrix words & bigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.763</td><td>0.77</td><td>Binary Matrix words & bigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.773</td><td>0.776</td><td>Binary Matrix words & bigrams</td><td>Logistic Regression</td></tr>
</table>

## Count Matrix words & bigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.747</td><td>0.740</td><td>words & bigrams Logistic Regression</td><td>Count Matrix</td></tr>
    <tr><td>0.733</td><td>0.725</td><td>words & bigrams Logistic Regression</td><td>Count Matrix</td></tr>
    <tr><td>0.727</td><td>0.726</td><td>words & bigrams Logistic Regression</td><td>Count Matrix</td></tr>
    <tr><td>0.711</td><td>0.706</td><td>words & bigrams Logistic Regression</td><td>Count Matrix</td></tr>
</table>

## TF-IDF Matrice words & bigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.783</td><td>0.778</td><td>TF-IDF Matrice words & bigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.782</td><td>0.785</td><td>TF-IDF Matrice words & bigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.791</td><td>0.792</td><td>TF-IDF Matrice words & bigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.688</td><td>0.675</td><td>TF-IDF Matricoe wrds & bigrams</td><td>Logistic Regression</td></tr>
</table>

## Binary Matrix bigrams & trigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.685</td><td>0.679</td><td>Binary Matrix bigrams & trigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.716</td><td>0.719</td><td>Binary Matrix bigrams & trigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.710</td><td>0.699</td><td>Binary Matrix bigrams & trigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.721</td><td>0.710</td><td>Binary Matrix bigrams & trigrams</td><td>Logistic Regression</td></tr>
</table>

## Count Matrix bigrams & trigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.661</td><td>0.657</td><td>Count Matrix bigrams & trigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.696</td><td>0.680</td><td>Count Matrix bigrams & trigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.682</td><td>0.681</td><td>Count Matrix bigrams & trigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.696</td><td>0.69</td><td>Count Matrix bigrams & trigrams</td><td>Logistic Regression</td></tr>
</table>

## TF-IDF Matrix bigrams & trigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.658</td><td>0.651</td><td>TF-IDF Matrix bigrams & trigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.696</td><td>0.680</td><td>TF-IDF Matrix bigrams & trigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.685</td><td>0.679</td><td>TF-IDF Matrix bigrams & trigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.696</td><td>0.69</td><td>TF-IDF Matrix bigrams & trigrams</td><td>Logistic Regression</td></tr>
</table>

## Binary Matrix words, bigrams & trigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.740</td><td>0.734</td><td>Binary Matrix words, bigrams & trigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.768</td><td>0.763</td><td>Binary Matrix words, bigrams & trigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.762</td><td>0.766</td><td>Binary Matrix words, bigrams & trigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.769</td><td>0.767</td><td>Binary Matrix words, bigrams & trigrams</td><td>Logistic Regression</td></tr>
</table>

## Count Matrix words, bigrams & trigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.714</td><td>0.723</td><td>Count Matrix words, bigrams & trigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.741</td><td>0.731</td><td>Count Matrix words, bigrams & trigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.731</td><td>0.720</td><td>Count Matrix words, bigrams & trigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.744</td><td>0.732</td><td>Count Matrix words, bigrams & trigrams</td><td>Logistic Regression</td></tr>
</table>


## TF-IDF Matrix words, bigrams & trigrams

<table>
    <tr><td>cross_val_accuracy</td><td>test_accuracy</td><td>data</td><td>model type</td></tr>
    <tr><td>0.774</td><td>0.750</td><td>TF-IDF Matrix words, bigrams & trigrams</td><td>SGDClassifier</td></tr>
    <tr><td>0.772</td><td>0.758</td><td>TF-IDF Matrix words, bigrams & trigrams</td><td>SVM LinearSVC</td></tr>
    <tr><td>0.784</td><td>0.776</td><td>TF-IDF Matrix words, bigrams & trigrams</td><td>Passivd Agressive Classifier</td></tr>
    <tr><td>0.658</td><td>0.64</td><td>TF-IDF Matrix words, bigrams & trigrams</td><td>Logistic Regression</td></tr>
</table>

**HADJ-ALI Sara**


